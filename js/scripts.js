$(document).ready(function() {
 
$("html, body").mousewheel(function(e, delta) { 
    $('html, body').stop().animate({scrollLeft: '-='+(500*delta)+'px' }, 300, 'easeOutQuint');
    e.preventDefault();
});


   $('#slider').s3Slider({
            timeOut: 3000
});

$("#button").click(function () {
    var effect = 'slide';
    var options = { direction: 'right' };
    var duration = 700;
	$('#title').fadeToggle(700);
    $('#menu-nav').toggle(effect, options, duration);
});

	
function initialize() {
var secheltLoc = new google.maps.LatLng(-26.038195,28.0175);
var sechelLang = new google.maps.LatLng (-26.0386433,28.0172885);	
var myMapOptions = {
zoom: 15
,center: sechelLang
,mapTypeId: google.maps.MapTypeId.ROADMAP
,styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]}]
};
var theMap = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);
var marker = new google.maps.Marker({
map: theMap,
draggable: true,
position: secheltLoc,
visible: true
});
}  
google.maps.event.addDomListener(window, 'load', initialize); 	

	
});

